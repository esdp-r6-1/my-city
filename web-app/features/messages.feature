# language: ru

Функционал: Управление сообщениями
		Как администратор или модератор
		Я хочу иметь возможность управлять сообщениями
		Чтобы менять их статус и иметь возможность удалить

	Предыстория:
		Допустим залогинен модератор с email "moder@moder.ru" и паролем "qweqweqwe"
		И он находится на главной странице
		Если он нажмет на кнопку "Редактировать" у сообщения "Тестовое сообщение"

	Сценарий: Изменение статуса сообщения
		Если на странице редактирования он сменит статус сообщения на "done"
		И нажмет на кнопку "Обновить статус"
		То он увидит сообщение "Статус сообщения успешно изменен!"
		И статус сообщения сменится на "done"

	Сценарий: Удаление сообщения
		Если на странице редактирования он нажмет на кнопку "Удалить сообщение"
		То он увидит сообщение "Сообщение успешно удалено!"
		И на странице с сообщениями больше не будет сообщения "Тестовое сообщение"

	Сценарий: Изменение категории сообщения
		Если на странице редактирования он изменит категорию на "Тестовая категория"
		И нажмет на кнопку "Обновить категорию"
		То он увидит сообщение "Категория сообщения успешна изменена"
		И категория сообщения сменится на "Тестовая категория"

